import React, {Component} from 'react';
import './App.css';

class App extends Component {
	componentDidMount() {
		const decoder = new TextDecoder();
		fetch("http://localhost:8080/employee").then((response) => {
			const reader = response.body.getReader();

			const processResult = (result) => {
				if (result.done) {
					console.log("Fetch complete");
					return;
				}
				this.setState(state => ({
					...state,
					data: [...state.data, JSON.parse(decoder.decode(result.value, { stream: true }))]
				}));

				return reader.read().then(processResult);
			};

			reader.read().then(processResult);
		});
	}

	constructor() {
		super();
		this.state = {
			data: []
		};
	}

	render() {
		let i = 0;
		return (
			<div>
				{ this.state.data.map(e => <p key={ i++ }>{ `id: ${ e.id } name: ${ e.name } age: ${ e.age }` }</p>) }
			</div>
		)
	}
}

export default App;
